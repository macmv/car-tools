local data_util = require("__flib__.data-util")

data:extend({
  {
    type = "selection-tool",
    name = "inserter-linter-car-selector",
    icon = "__car-tools__/graphics/icon/car.png",
    icon_size = 102,
    scale = 2,
    selection_color = { r = 1, g = 1, b = 0 },
    alt_selection_color = { r = 1, g = 1, b = 0 },
    selection_cursor_box_type = "entity",
    alt_selection_cursor_box_type = "entity",
    selection_mode = { "entity-with-health", "friend" },
    alt_selection_mode = { "entity-with-health", "friend" },
    entity_type_filters = { "car" },
    alt_entity_type_filters = { "car" },
    stack_size = 1,
    flags = { "hidden", "only-in-cursor", "not-stackable" },
    draw_label_for_cursor_render = true,
  },
})

data:extend({
  {
    type = "custom-input",
    name = "car-tools-get-car-tool",
    key_sequence = "N",
  },
})

local styles = data.raw["gui-style"].default

styles["car-tools-amount-label"] = {
  type = "label_style",
  parent = "label",
  horizontal_align = "center",
}
styles["car-tools-column-label"] = {
  type = "label_style",
  parent = "bold_label",
  horizontal_align = "center",
}

styles["car-tools-scroll-pane"] = {
  type = "scroll_pane_style",
  extra_padding_when_activated = 0,
  padding = 0,
  horizontally_stretchable = "on",
  minimal_height = 45,
  maximal_height = 45 * 9,
  graphical_set = {
    shadow = default_inner_shadow,
  },
  vertical_flow_style = {
    type = "vertical_flow_style",
    vertical_spacing = 0,
  },
}

styles["car-tools-row-even"] = {
  type = "frame_style",
  parent = "statistics_table_item_frame",
  top_padding = 2,
  bottom_padding = 2,
  left_padding = 8,
  height = 45,
  horizontally_stretchable = "on",
  horizontal_flow_style = {
    type = "horizontal_flow_style",
    vertical_align = "center",
    horizontal_spacing = 12,
  },
  graphical_set = {},
}

styles["car-tools-row-odd"] = {
  type = "frame_style",
  parent = "car-tools-row-even",
  graphical_set = {
    base = {
      center = { position = { 472, 25 }, size = { 1, 1 } },
    },
  },
}


styles["car-tools-toolbar-frame"] = {
  type = "frame_style",
  parent = "subheader_frame",
  left_padding = 8,
  right_padding = 8,
  horizontal_flow_style = {
    type = "horizontal_flow_style",
    horizontal_spacing = 12,
    vertical_align = "center",
  },
}

styles["car-tools-tools-flow"] = {
  type = "vertical_flow_style",
  horizontal_align = "left",
  vertically_stretchable = "on",
  left_padding = 12,
  right_padding = 12,
  bottom_padding = 12,
}
styles["car-tools-button"] = {
  type = "button_style",
  parent = "button",
  width = 140,
}
styles["car-tools-red-button"] = {
  type = "button_style",
  parent = "red_button",
  width = 140,
}
