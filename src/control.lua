local gui = require("__flib__.gui-beta")
local math = require("__flib__.math")
local event = require("__flib__.event")

function say_hi()
  game.print("worked")
end

event.register("car-tools-get-car-tool", function(e)
  local player = game.get_player(e.player_index)
  player.cursor_stack.set_stack({ name = "inserter-linter-car-selector", count = 1 })
end)

open_windows = {}

event.register({
  defines.events.on_player_selected_area,
  defines.events.on_player_alt_selected_area
}, function(e)
  local player = game.get_player(e.player_index)

  if open_windows[e.player_index] == nil then
    open_windows[e.player_index] = build_window(player)
  end

  local gui = open_windows[e.player_index]
  gui.cars = e.entities
  gui.refs.window.visible = true
  gui.refs.window.force_auto_center()
  player.opened = gui.refs.window
  gui:count_items()
end)

event.on_tick(function(e)
  for _, gui in pairs(open_windows) do
    if gui.refs.window.visible then
      if gui.state.auto_refresh then
        gui:count_items()
      end
    end
  end
end)

local function format_caption(amount)
  postfix = ""
  if amount > 1000000 then
    postfix = "m"
    amount = amount / 1000000
  elseif amount > 1000 then
    postfix = "k"
    amount = amount / 1000
  end
  rounded = math.round_to(amount, 2)
  return tostring(rounded)..postfix
end
local function format_tooltip(amount, after)
  return math.round_to(amount, 3)..after
end
local function label(width)
  return {
    type = "label",
    style = "car-tools-amount-label",
    style_mods = {
      width = width,
      font_color = { r = 1, g = 1, b = 1 }
    },
  }
end
local function add_empty_column(index, scroll_pane)
  return gui.add(scroll_pane, {
    type = "frame",
    style = "car-tools-row-"..(index % 2 == 0 and "even" or "odd"),
    {
      type = "sprite-button",
      style = "transparent_slot",
      style_mods = { width = 40 },
    },
    label(60),
    label(60),
    label(60),
  })
end
local function update_column(frame, amounts)
  if amounts.total_filtered ~= 0 then
    percent = (amounts.total / amounts.total_filtered) * 100
  else
    percent = 0
  end
  gui.update(frame, {
    {
      elem_mods = {
        sprite = "item/"..amounts.item,
        tooltip = amounts.item,
      },
    },
    {
      elem_mods = {
        caption = format_caption(amounts.total),
        tooltip = format_tooltip(amounts.total, " total"),
      },
    },
    {
      elem_mods = {
        caption = format_caption(amounts.total_filtered),
        tooltip = format_tooltip(amounts.total_filtered, " filtered"),
      },
    },
    {
      elem_mods = {
        caption = format_caption(percent).."%",
        tooltip = format_tooltip(percent, "%"),
      },
    },
  })
end
function update_columns(refs, sorted)
  local index = 1
  for _, amounts in ipairs(sorted) do
    local frame = refs.scroll_pane.children[index]
    if not frame then
      frame = add_empty_column(index, refs.scroll_pane)
    end
    update_column(frame, amounts)
    index = index + 1
  end
  while index <= #refs.scroll_pane.children do
    refs.scroll_pane.children[index].destroy()
  end
end

local stack_size_cache = {}
function find_stack_size(item)
  if stack_size_cache[item] ~= nil then
    return stack_size_cache[item]
  else
    stack_size = game.item_prototypes[item].stack_size
    stack_size_cache[item] = stack_size
    return stack_size
  end
end

local CarGui = {}
function CarGui:count_items()
  -- key: item name
  -- value: { total, total_filtered }
  local totals = {}

  for i, car in ipairs(self.cars) do
    local inv = car.get_output_inventory()
    for j=1,#inv do
      local item = inv[j]
      if item.count ~= 0 then
        if totals[item.name] == nil then
          totals[item.name] = {
            total = 0,
            total_filtered = 0,
          }
        end
        local t = totals[item.name]
        t.total = t.total + item.count
      end
      local filter = inv.get_filter(j)
      if filter ~= nil then
        if totals[filter] == nil then
          totals[filter] = {
            total = 0,
            total_filtered = 0,
          }
        end
        local t = totals[filter]
        t.total_filtered = t.total_filtered + find_stack_size(filter)
      end
    end
  end

  -- key: index
  -- value: { item = item name, total, total_filtered }
  local sorted = {}
  local index = 1
  for item, obj in pairs(totals) do
    obj.item = item
    sorted[index] = obj
    index = index + 1
  end

  function compare(a, b)
    return a.total > b.total
  end
  table.sort(sorted, compare)

  update_columns(self.refs, sorted)
end

local actions = {}

function actions.close(gui, _)
  gui.refs.window.visible = false
  gui.player.opened = nil
end
function actions.refresh(gui, _)
  gui:count_items()
end
function actions.toggle_auto_refresh(gui, _)
  gui.state.auto_refresh = not gui.state.auto_refresh
  gui.refs.auto_refresh_checkbox = gui.state.auto_refresh
end
function actions.delete_contents(gui, _)
  gui.player.play_sound({ path = "utility/paste_activated" })
  for _, car in ipairs(gui.cars) do
    car.get_output_inventory().clear()
  end
end
function actions.delete_selection(gui, _)
  gui.player.play_sound({ path = "utility/paste_activated" })
  for _, car in ipairs(gui.cars) do
    car.destroy()
  end
end

gui.hook_events(function(e)
  local action = gui.read_action(e)
  if action then
    local window = open_windows[e.player_index]
    if window then
      local handler = actions[action]
      if handler then
        handler(window, e)
      else
        game.print("unknown action "..action)
      end
    end
  end
end)

local function frame_action_button(sprite, tooltip, action, ref)
  return {
    type = "sprite-button",
    style = "frame_action_button",
    sprite = sprite .. "_white",
    hovered_sprite = sprite .. "_black",
    clicked_sprite = sprite .. "_black",
    tooltip = tooltip,
    mouse_button_filter = { "left" },
    ref = ref,
    actions = {
      on_click = action,
    },
  }
end

function build_window(player)
  -- Guis persist accross saves. I could not be bothered to deal with that,
  -- so this means we will re-create the screen once per save load (which is
  -- perfectly reasonable).
  for i, child in ipairs(player.gui.screen.children) do
    if child.name == "car-tools-window" then
      child.destroy()
      break
    end
  end
  local refs = gui.build(player.gui.screen, {
    {
      type = "frame",
      name = "car-tools-window",
      direction = "vertical",
      visible = false,
      ref = { "window" },
      actions = {
        on_closed = "close",
      },
      {
        type = "flow",
        style = "flib_titlebar_flow",
        ref = { "titlebar_flow" },
        {
          type = "label",
          style = "frame_title",
          style_mods = { left_margin = 4 },
          caption = { "car-tools-gui.title" },
          ignored_by_interaction = true,
        },
        { type = "empty-widget", style = "flib_titlebar_drag_handle", ignored_by_interaction = true },
        {
          type = "textfield",
          style_mods = { top_margin = -3, width = 150 },
          visible = false,
          ref = { "search_textfield" },
          actions = {
            on_text_changed = "update_search_query",
          },
        },
        frame_action_button("utility/search", { "car-tools-gui.rcalc-search-instruction" }, "toggle_search", { "search_button" }),
        -- frame_action_button("rcalc_pin", { "car-tools-gui.rcalc-keep-open" }, "toggle_pinned", { "pin_button" }),
        frame_action_button("utility/close", { "car-tools-gui.close-instruction" }, "close"),
      },
      {
        type = "frame",
        style = "inside_shallow_frame",
        direction = "vertical",
        -- {
        --   type = "frame",
        --   style = "car-tools_toolbar_frame",
        --   { type = "label", style = "subheader_caption_label", caption = { "car-tools-gui.rcalc-measure-label" } },
        --   {
        --     type = "drop-down",
        --     items = {},
        --     ref = { "measure_dropdown" },
        --     actions = {
        --       on_selection_state_changed = "update_measure",
        --     },
        --   },
        --   { type = "empty-widget", style = "flib_horizontal_pusher" },
        --   { type = "label", style = "caption_label", caption = { "car-tools-gui.rcalc-units-label" } },
        --   {
        --     type = "choose-elem-button",
        --     style = "rcalc_units_choose_elem_button",
        --     style_mods = { right_margin = -8 },
        --     elem_type = "entity",
        --     ref = { "units_button" },
        --     actions = {
        --       on_elem_changed = "update_units_button",
        --     },
        --   },
        --   {
        --     type = "choose-elem-button",
        --     style = "rcalc_units_choose_elem_button",
        --     style_mods = { right_margin = -8 },
        --     elem_type = "entity",
        --     elem_mods = { locked = true },
        --     ref = { "selection_tool_button" },
        --     actions = {
        --       on_click = "give_selection_tool",
        --     },
        --   },
        --   {
        --     type = "drop-down",
        --     ref = { "units_dropdown" },
        --     actions = {
        --       on_selection_state_changed = "update_units_dropdown",
        --     },
        --   },
        -- },
        {
          type = "flow",
          style_mods = { padding = 12, margin = 0 },
          {
            type = "frame",
            style = "deep_frame_in_shallow_frame",
            direction = "vertical",
            ref = { "list_frame" },
            {
              type = "frame",
              style = "car-tools-toolbar-frame",
              { type = "label", style = "car-tools-column-label", style_mods = { width = 40 }, caption = { "car-tools-gui.item" } },
              { type = "label", style = "car-tools-column-label", style_mods = { width = 60 }, caption = { "car-tools-gui.total" } },
              { type = "label", style = "car-tools-column-label", style_mods = { width = 60 }, caption = { "car-tools-gui.filtered" } },
              { type = "label", style = "car-tools-column-label", style_mods = { width = 60 }, caption = { "car-tools-gui.percent" } },
            },
            {
              type = "scroll-pane",
              style = "car-tools-scroll-pane",
              horizontal_scroll_policy = "never",
              ref = { "scroll_pane" },
            },
            -- {
            --   type = "flow",
            --   style = "rcalc_warning_flow",
            --   visible = false,
            --   ref = { "warning_flow" },
            --   {
            --     type = "label",
            --     style = "bold_label",
            --     caption = { "car-tools-gui.rcalc-click-to-select-inserter" },
            --     ref = { "warning_label" },
            --   },
            -- },
            -- {
            --   type = "frame",
            --   style = "rcalc_totals_frame",
            --   ref = { "totals_frame" },
            --   { type = "label", style = "caption_label", caption = { "car-tools-gui.rcalc-totals-label" } },
            --   { type = "empty-widget", style = "flib_horizontal_pusher" },
            --   total_label({ "car-tools-gui.rcalc-output-label" }),
            --   { type = "empty-widget", style = "flib_horizontal_pusher" },
            --   total_label({ "car-tools-gui.rcalc-input-label" }),
            --   { type = "empty-widget", style = "flib_horizontal_pusher" },
            --   total_label({ "car-tools-gui.rcalc-net-label" }),
            -- },
          },
        },
        {
          type = "flow",
          style = "car-tools-tools-flow",
          direction = "vertical",
          -- {
          --   type = "frame",
          --   style_mods = { horizontally_stretchable = "on", horizontal_align = "center" },
          --   {
          --     type = "label",
          --     style = "frame_title",
          --     caption = { "car-tools-gui.tools-label" },
          --   }
          -- },
          {
            type = "checkbox",
            style = "caption_checkbox",
            actions = {
              on_click = "toggle_auto_refresh",
            },
            ref = { "auto_refresh_checkbox" },
            state = false,
            caption = { "car-tools-gui.auto-refresh" },
          },
          {
            type = "button",
            style = "car-tools-button",
            actions = {
              on_click = "refresh",
            },
            caption = { "car-tools-gui.refresh" },
          },
          {
            type = "button",
            style = "car-tools-button",
            actions = {
              on_click = "delete_contents",
            },
            caption = { "car-tools-gui.delete-contents" },
          },
          {
            type = "button",
            style = "car-tools-red-button",
            actions = {
              on_click = "delete_selection",
            },
            caption = { "car-tools-gui.delete-selection" },
          },
          -- {
          --   type = "slider",
          --   style = "car-tools_multiplier_slider",
          --   minimum_value = 1,
          --   maximum_value = 100,
          --   value_step = 1,
          --   ref = { "multiplier_slider" },
          --   actions = {
          --     on_value_changed = "update_multiplier_slider",
          --   },
          -- },
          -- {
          --   type = "textfield",
          --   style = "car-tools_multiplier_textfield",
          --   numeric = true,
          --   allow_decimal = true,
          --   clear_and_focus_on_right_click = true,
          --   lose_focus_on_confirm = true,
          --   text = "1",
          --   ref = { "multiplier_textfield" },
          --   actions = {
          --     on_text_changed = "update_multiplier_textfield",
          --   },
          -- },
        },
      },
    },
  })

  refs.titlebar_flow.drag_target = refs.window
  refs.window.force_auto_center()

  local gui = {
    refs = refs,
    player = player,
    state = {
      auto_refresh = false,
    }
  }

  setmetatable(gui, { __index = CarGui })

  return gui
end
